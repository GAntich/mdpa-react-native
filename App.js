import React, { Component } from 'react';
import { Alert, StyleSheet, Text, View, Image, ScrollView, TouchableOpacity, } from 'react-native';
import { sprintf } from 'sprintf-js';
import { BlurView, VibrancyView } from 'react-native-blur';
import * as moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';

const data = [
  {
      image: 'https://images.unsplash.com/photo-1429681601148-75510b2cef43?auto=compress,format&fit=crop&w=640&h=480&q=60',
      location: 'Café Kahvipuu, Kokkola',
      author: 'Seemi Samuel',
      likes: 8,
      date: 1429681601148
  },
  {
      image: 'https://images.unsplash.com/photo-1453614512568-c4024d13c247?dpr=1&auto=compress,format&fit=crop&w=376&h=250&q=60',
      location: 'Bintaro, Indonesia',
      author: 'Nafinia Putra',
      likes: 43,
      date: 1453614512568
  },
  {
      image: 'https://images.unsplash.com/photo-1489396944453-834c536105c0?dpr=1&auto=compress,format&fit=crop&w=376&h=251&q=60',
      location: 'Bodø, Bodo, Norway',
      author: 'Thomas Litangen',
      likes: 17,
      date: 1489396944453
  },
  {
      image: 'https://images.unsplash.com/5/unsplash-bonus.jpg?auto=compress,format&fit=crop&w=640&h=480&q=60',
      location: 'Kitsuné Café, Montreal',
      author: 'Luke Chesser',
      likes: 3,
      date: 1489396944434
  },
  {
      image: 'https://images.unsplash.com/photo-1452776145041-517a74be1f14?dpr=1&auto=compress,format&fit=crop&w=376&h=250&q=60',
      location: '26 Rathbone Pl, London',
      author: 'Jordan Sanchez',
      likes: 23,
      date: 1452776145041
  },
  {
      image: 'https://images.unsplash.com/photo-1441336558687-a06957a7642a?dpr=1&auto=compress,format&fit=crop&w=376&h=250&q=60',
      location: 'Iconoclast Koffiehuis, Edmonton',
      author: 'Redd Angelo',
      likes: 6,
      date: 1441336558687
  }
]

class CoffeBar extends Component {
    state = {
        likes: this.props.likes
    };


    addLike = () => {
      this.setState({ likes: this.state.likes + 1 }, () => {
        console.log(sprintf('Like is %1$s', this.state.likes));
      });
    };

    showAlert = () => {
        Alert.alert('You have exceeded the number of Likes allowed')
    };


    render() {
        //MOMENT.JS
        var moment = require('moment');
        //var date=this.props.image.substring(str.lastIndexOf("-")+1,str.lastIndexOf("-"));
        //As not all strings contain timestamp, new "date" value was created in data.
        //If strings all contained timepstamp, var date subrstring above would have been used.
        var formattedDate = moment(this.props.date).format("DD-MM-YYYY");
        //VECTOR-ICONS
        const likeIcon = (<Icon name="rocket" size={30} color="#900" />)

        return (
            <View style={styles.wrapper}>
                <Image style={styles.image} source={{ uri: this.props.image }} />
                <Text style={styles.likes}>{this.state.likes}</Text>
                <BlurView style={styles.bottomWrapper} blurType="light" blurAmount={2}>
                    <Text style={styles.location}>{this.props.location}</Text>
                    <Text style={styles.author}>{this.props.author + " on " + formattedDate}</Text>
                    {this.state.likes < 10 ? (
                        <TouchableOpacity style={styles.button} onPress={this.addLike}>
                            <Text style={styles.buttonText}><Icon name="thumbs-o-up" size={16}/> Like</Text>
                        </TouchableOpacity>
                    ) : 
                        <TouchableOpacity style={styles.buttonDissabled} onPress={this.showAlert}>
                            <Text style={styles.buttonTextDissabled}><Icon name="thumbs-o-up" size={16}/> Like</Text>
                        </TouchableOpacity>
                    }
                </BlurView>
            </View>
        );
    }
}

export default class App extends Component {
    render() {
        return (
        <ScrollView contentContainerStyle={styles.scrollContent}>
        {data.map((entry, idx) => {
          return (
            <CoffeBar 
              key={idx}
              {...entry}
            />
          );
        })}
        </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    wrapper: {
    },
    scrollContent: {
        paddingTop: 20
    },
    bottomWrapper: {
        position: 'absolute',
        justifyContent: 'center',
        height: 50,
        backgroundColor: '#0009',
        paddingLeft: 10,
        width: '100%',
        bottom: 0
    },
    image: {
        width: '100%',
        height: 250
    },
    likes: {
        position: 'absolute',
        color: '#fff',
        fontSize: 20,
        borderColor: '#fff',
        borderWidth: 1,
        borderRadius: 15,
        overflow: 'hidden',
        textAlign: 'center',
        minWidth: 30,
        height: 30,
        top: 10,
        right: 10,
        backgroundColor: '#0009'
    },
    author: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold'
    },
    location: {
        color: '#fff',
        fontSize: 16
    },
    button: {
        position: 'absolute',
        right: 10,
        height: 40,
        width: 80,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#304582',
        borderRadius: 5
    },
    buttonText: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
    buttonDissabled: {
        position: 'absolute',
        right: 10,
        height: 40,
        width: 80,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#1a2547',
        borderRadius: 5
    },
    buttonTextDissabled: {
        color: '#555',
        fontSize: 16,
        fontWeight: 'bold',
        //textDecorationLine: 'line-through'
    }
});